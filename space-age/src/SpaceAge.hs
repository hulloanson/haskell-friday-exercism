module SpaceAge (Planet(..), ageOn) where

data Planet = Mercury
            | Venus
            | Earth
            | Mars
            | Jupiter
            | Saturn
            | Uranus
            | Neptune deriving (Enum, Eq)

ageOn :: Planet -> Float -> Float
planetYearSeconds :: Float -> Float
planetYearSeconds toEarthRatio = 31557600 * toEarthRatio
ageOn planet seconds = case planet of
  Mercury -> seconds / planetYearSeconds 0.2408467
  _ | planet == Venus -> seconds / planetYearSeconds 0.61519726
  _ | planet == Earth -> seconds / planetYearSeconds 1.0
  _ | planet == Mars -> seconds / planetYearSeconds 1.8808158
  _ | planet == Jupiter -> seconds / planetYearSeconds 11.862615
  _ | planet == Saturn -> seconds / planetYearSeconds 29.447498
  _ | planet == Uranus -> seconds / planetYearSeconds 84.016846
  _ | planet == Neptune -> seconds / planetYearSeconds 164.79132
  _ | otherwise -> 0
