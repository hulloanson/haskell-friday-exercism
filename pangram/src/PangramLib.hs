module PangramLib (hasChar, isPangram, foldRight) where

import Data.Char (toLower)

-- myMap fn (x:xs) = fn x : myMap fn xs

-- myMap fn [] = []

myFold :: (a -> c -> c) -> [a] -> c -> c
myFold fn (s:xs) i = myFold fn xs (fn s i) 
myFold _ [] i = i

foldRight :: (a -> c -> c) -> [a] -> c -> c
foldRight fn (x:xs) i = fn x (foldRight fn xs i)
foldRight _ [] i = i

-- aInt = ord 'a'

-- minusA :: Char -> Int
-- minusA c = ord (toLower c) - aInt

-- -- addIfMatch :: Num p => Char -> p -> Int -> p
-- addIfMatch c count i = case i of
--   _ | minusA c == i -> count + 1
--   _ | otherwise -> count

-- a2zcount :: [Int] -> Char -> Int -> [Int]
-- a2zcount counts c _ = myMap (addIfMatch c) counts

-- a2zcount 

-- countSentence sentence counts = map 

-- hasChar (s:xs) char = toLower s == toLower char || hasChar xs char
-- hasChar [] _  = False

charEq :: Char -> Char -> Bool -> Bool
charEq c1 c2 currVal = toLower c1 == toLower c2 || currVal

hasChar :: [Char] -> Char -> Bool
hasChar sentence char = myFold (charEq char) sentence False

-- containsAllOf (s:xs) inAnotherSentence = hasChar inAnotherSentence s && containsAllOf xs inAnotherSentence
-- containsAllOf [] _  = True

foldableHasChar :: [Char] -> Char -> Bool -> Bool
foldableHasChar sentenceToInspect c currVal = hasChar sentenceToInspect c && currVal

containsAllOf :: [Char] -> [Char] -> Bool
containsAllOf sentence inAnotherSentence = myFold (foldableHasChar inAnotherSentence) sentence True

isPangram :: [Char] -> Bool
isPangram = containsAllOf "abcdefghijklmnopqrstuvwxyz"

-- hasChar (s:xs) char = case s of
--   _ | s == char -> True
--   _ | otherwise -> hasChar xs char